/* Copyright (C) 2016  Tobias Bielefeld
 * Copyright (C) 2016  Tobias Bielefeld
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * If you want to contact me, send me an e-mail at tobias.bielefeld@gmail.com
 */

package de.tobiasbielefeld.solitaire.dialogs;

import android.Manifest;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.viewpagerindicator.CirclePageIndicator;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

//import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;
import de.tobiasbielefeld.solitaire.LoadImahe;
import de.tobiasbielefeld.solitaire.PagerAdapter;
import de.tobiasbielefeld.solitaire.PagerModel;
import de.tobiasbielefeld.solitaire.R;
import de.tobiasbielefeld.solitaire.classes.Card;
import de.tobiasbielefeld.solitaire.classes.CustomDialogFragment;
import de.tobiasbielefeld.solitaire.classes.CustomImageView;
import de.tobiasbielefeld.solitaire.ui.GameManager;
import de.tobiasbielefeld.solitaire.ui.manual.Manual;
import de.tobiasbielefeld.solitaire.ui.settings.Settings;

import static android.content.Context.MODE_MULTI_PROCESS;
import static de.tobiasbielefeld.solitaire.LoadImahe.BACKGROUND_PREFERENCES_USER;
import static de.tobiasbielefeld.solitaire.LoadImahe.PREFERENCES_CONSTANTS;
import static de.tobiasbielefeld.solitaire.SharedData.GAME;
import static de.tobiasbielefeld.solitaire.SharedData.autoMove;
import static de.tobiasbielefeld.solitaire.SharedData.bitmaps;
import static de.tobiasbielefeld.solitaire.SharedData.cards;
import static de.tobiasbielefeld.solitaire.SharedData.currentGame;
import static de.tobiasbielefeld.solitaire.SharedData.gameLogic;
import static de.tobiasbielefeld.solitaire.SharedData.hint;
import static de.tobiasbielefeld.solitaire.SharedData.lg;
import static de.tobiasbielefeld.solitaire.SharedData.prefs;
import static de.tobiasbielefeld.solitaire.SharedData.showToast;
import static de.tobiasbielefeld.solitaire.ui.GameManager.BACKGROUND_PREFERENCES_BACK;

/**
 * dialog to handle new games or returning to main menu( in that case, cancel the current activity)
 */

public class DialogInGameHelpMenu extends CustomDialogFragment {
    private static final int REQUEST_PHONE_CALL = 1;
    private ImageView image;
    private static int NUMBER_OF_CARD_THEMES = 5;
    private static int THEME_BACKGROUNDS = 6;
    GameManager gameManager;
    private TypedValue typedValue = new TypedValue();
    private static int NUMBER_OF_CARD_BACKGROUNDS = 5;
    private static int RESULT_LOAD_IMG = 1;
    private LinearLayout[] linearLayoutsBackgrounds = new LinearLayout[NUMBER_OF_CARD_BACKGROUNDS];
    private ImageView[] imageViews = new ImageView[NUMBER_OF_CARD_BACKGROUNDS];


    private DialogPreferenceCards preferenceCards;
    private LinearLayout[] linearLayouts = new LinearLayout[NUMBER_OF_CARD_THEMES];
    String imgDecodableString;
    SharedPreferences mSettings;
    //public static final String BACKGROUND_PREFERENCES = "background";
    public static final String BACKGROUND_PREFERENCES_URL = "URL";
    public static final String BACKGROUND_PREFERENCES_FILE = "FILE";

    private int selectedBackground;
    private int selectedBackgroundColor;
    private ImageButton[] buttonsB = new ImageButton[THEME_BACKGROUNDS];


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Dialog dialog = new Dialog(getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.support_menu);
        gameManager = (GameManager) getActivity();
        mSettings = getActivity().getSharedPreferences(PREFERENCES_CONSTANTS,MODE_MULTI_PROCESS);
        imgDecodableString = mSettings.getString(BACKGROUND_PREFERENCES_URL, "");
        ImageButton buttonExit = (ImageButton) dialog.findViewById(R.id.exit);
        buttonExit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();

            }
        });

        Button autoMoveB = (Button) dialog.findViewById(R.id.auto_move);
        autoMoveB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!gameLogic.hasWon()) {
                    autoMove.start();
                    dialog.cancel();
                }
            }
        });

        Button hintB = (Button) dialog.findViewById(R.id.hint);
        hintB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!gameLogic.hasWon()) {
                    hint.showHint();
                    dialog.cancel();
                }
            }
        });

        Button mixB = (Button) dialog.findViewById(R.id.mix_cards);
        mixB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!gameLogic.hasWon()) {
                    if (currentGame.hintTest() == null) {
                        if (prefs.getShowDialogMixCards()) {
                            prefs.putShowDialogMixCards(false);
                            DialogMixCards dialogMixCards = new DialogMixCards();
                            dialogMixCards.show(getFragmentManager(), "MIX_DIALOG");
                            dialog.cancel();
                        } else {
                            currentGame.mixCards();
                            dialog.cancel();
                        }
                    } else {
                        showToast(getString(R.string.dialog_mix_cards_not_available), getActivity());
                        dialog.cancel();
                    }
                }
            }
        });

        LinearLayout themesB = (LinearLayout) dialog.findViewById(R.id.thems);
        themesB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog2 = new Dialog(getActivity());
                dialog2.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog2.setContentView(R.layout.dialog_settings_cards);



                ImageButton exitB = (ImageButton) dialog2.findViewById(R.id.exit);
                exitB.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog2.cancel();
                    }
                });

                buttonsB[0] = (ImageButton) dialog2.findViewById(R.id.bB1);
                buttonsB[1] = (ImageButton) dialog2.findViewById(R.id.bB2);
                buttonsB[2] = (ImageButton) dialog2.findViewById(R.id.bB3);
                buttonsB[3] = (ImageButton) dialog2.findViewById(R.id.bB4);
                buttonsB[4] = (ImageButton) dialog2.findViewById(R.id.bB5);
                buttonsB[5] = (ImageButton) dialog2.findViewById(R.id.bB6);
                final RelativeLayout imgViewrs = (RelativeLayout) getActivity().findViewById(R.id.mainRelativeLayoutBackground);
                for (int i = 0; i < THEME_BACKGROUNDS; i++) {
                    buttonsB[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String filename;
                            switch (v.getId()) {

                                case R.id.bB1:
                                default:

                                    filename = "background_a.webp";
                                    InputStream inputStream = null;
                                    try{
                                        inputStream = getContext().getAssets().open(filename);
                                        Drawable d = Drawable.createFromStream(inputStream, null);
                                        imgViewrs.setBackground(d);
                                        //imgView.setScaleType(ImageView.ScaleType.FIT_XY);
                                    }
                                    catch (IOException e){
                                        e.printStackTrace();
                                    }
                                    finally {
                                        try{
                                            if(inputStream!=null)
                                                inputStream.close();
                                        }
                                        catch (IOException ex){
                                            ex.printStackTrace();
                                        }
                                    }
                                    break;
                                case R.id.bB2:
                                    filename = "background_b.webp";
                                    InputStream inputStreamb = null;
                                    try{
                                        inputStreamb = getContext().getAssets().open(filename);
                                        Drawable d = Drawable.createFromStream(inputStreamb, null);
                                        imgViewrs.setBackground(d);
                                        //imgView.setScaleType(ImageView.ScaleType.FIT_XY);
                                    }
                                    catch (IOException e){
                                        e.printStackTrace();
                                    }
                                    finally {
                                        try{
                                            if(inputStreamb!=null)
                                                inputStreamb.close();
                                        }
                                        catch (IOException ex){
                                            ex.printStackTrace();
                                        }
                                    }
                                    break;
                                case R.id.bB3:
                                    filename = "background_d.webp";
                                    InputStream inputStreamd = null;
                                    try{
                                        inputStreamd = getContext().getAssets().open(filename);
                                        Drawable d = Drawable.createFromStream(inputStreamd, null);
                                        imgViewrs.setBackground(d);
                                        //imgView.setScaleType(ImageView.ScaleType.FIT_XY);
                                    }
                                    catch (IOException e){
                                        e.printStackTrace();
                                    }
                                    finally {
                                        try{
                                            if(inputStreamd!=null)
                                                inputStreamd.close();
                                        }
                                        catch (IOException ex){
                                            ex.printStackTrace();
                                        }
                                    }
                                    break;

                                case R.id.bB4:
                                    filename = "orange.webp";
                                    InputStream inputStreamo = null;
                                    try{
                                        inputStreamo = getContext().getAssets().open(filename);
                                        Drawable d = Drawable.createFromStream(inputStreamo, null);
                                        imgViewrs.setBackground(d);
                                        //imgView.setScaleType(ImageView.ScaleType.FIT_XY);
                                    }
                                    catch (IOException e){
                                        e.printStackTrace();
                                    }
                                    finally {
                                        try{
                                            if(inputStreamo!=null)
                                                inputStreamo.close();
                                        }
                                        catch (IOException ex){
                                            ex.printStackTrace();
                                        }
                                    }
                                    break;

                                case R.id.bB5:
                                    filename = "red.webp";
                                    InputStream inputStreamr = null;
                                    try{
                                        inputStreamr = getContext().getAssets().open(filename);
                                        Drawable d = Drawable.createFromStream(inputStreamr, null);
                                        imgViewrs.setBackground(d);
                                        //imgView.setScaleType(ImageView.ScaleType.FIT_XY);
                                    }
                                    catch (IOException e){
                                        e.printStackTrace();
                                    }
                                    finally {
                                        try{
                                            if(inputStreamr!=null)
                                                inputStreamr.close();
                                        }
                                        catch (IOException ex){
                                            ex.printStackTrace();
                                        }
                                    }
                                    break;

                                case R.id.bB6:
                                    filename = "green.webp";
                                    InputStream inputStreamg = null;
                                    try{
                                        inputStreamg = getContext().getAssets().open(filename);
                                        Drawable d = Drawable.createFromStream(inputStreamg, null);
                                        imgViewrs.setBackground(d);
                                        //imgView.setScaleType(ImageView.ScaleType.FIT_XY);
                                    }
                                    catch (IOException e){
                                        e.printStackTrace();
                                    }
                                    finally {
                                        try{
                                            if(inputStreamg!=null)
                                                inputStreamg.close();
                                        }
                                        catch (IOException ex){
                                            ex.printStackTrace();
                                        }
                                    }
                                    break;

                            }
                            InputStream inputStreamr = null;
                            try{
                                inputStreamr = getContext().getAssets().open(filename);
                                Drawable d = Drawable.createFromStream(inputStreamr, null);
                                imgViewrs.setBackground(d);
                                //imgView.setScaleType(ImageView.ScaleType.FIT_XY);
                            }
                            catch (IOException e){
                                e.printStackTrace();
                            }
                            finally {
                                try{
                                    if(inputStreamr!=null)
                                        inputStreamr.close();
                                }
                                catch (IOException ex){
                                    ex.printStackTrace();
                                }
                            }
                            SharedPreferences editor = getActivity().getSharedPreferences(PREFERENCES_CONSTANTS,MODE_MULTI_PROCESS);
                            editor.edit().putString(BACKGROUND_PREFERENCES_FILE, filename).apply();
                            editor.edit().putBoolean(BACKGROUND_PREFERENCES_USER, false).apply();
                            dialog2.cancel();


                        }
                    });
                }


                Button setCustom = (Button) dialog2.findViewById(R.id.settingsCardBackground5);
                setCustom.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (ContextCompat.checkSelfPermission(gameManager, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                                ActivityCompat.requestPermissions(gameManager, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, REQUEST_PHONE_CALL);

                            } else {
                                Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                // Start the Intentz
                                startActivityForResult(galleryIntent, RESULT_LOAD_IMG);

                            }
                        }


                    }
                });

                linearLayoutsBackgrounds[0] = (LinearLayout) dialog2.findViewById(R.id.settingsCardBackground0);
                linearLayoutsBackgrounds[1] = (LinearLayout) dialog2.findViewById(R.id.settingsCardBackground1);
                linearLayoutsBackgrounds[2] = (LinearLayout) dialog2.findViewById(R.id.settingsCardBackground2);
                linearLayoutsBackgrounds[3] = (LinearLayout) dialog2.findViewById(R.id.settingsCardBackground3);
                linearLayoutsBackgrounds[4] = (LinearLayout) dialog2.findViewById(R.id.settingsCardBackground4);
                //linearLayoutsBackgrounds[5] = (LinearLayout) dialog.findViewById(R.id.settingsCardBackground5);

                for (int i = 0; i < NUMBER_OF_CARD_BACKGROUNDS; i++) {
                    linearLayoutsBackgrounds[i].setOnClickListener(this);
                    imageViews[i] = (ImageView) linearLayoutsBackgrounds[i].getChildAt(0);
                }

                selectedBackground = prefs.getSavedCardBackground();

                for (int i = 0; i < NUMBER_OF_CARD_BACKGROUNDS; i++) {
                    linearLayoutsBackgrounds[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int choices = 0;
                            switch (v.getId()) {
                                case R.id.settingsCardBackground0:
                                default:
                                    choices = 0;
                                    selectedBackground = 0;
//                                    Card.updateCardBackgroundChoice();
                                    break;
                                case R.id.settingsCardBackground1:
                                    choices = 1;
                                    selectedBackground = 1;
//                                    Card.updateCardBackgroundChoice();
                                    break;
                                case R.id.settingsCardBackground2:
                                    choices = 2;
                                    selectedBackground = 2;
//                                    Card.updateCardBackgroundChoice();
                                    break;
                                case R.id.settingsCardBackground3:
                                    choices = 3;
                                    selectedBackground = 3;
//                                    Card.updateCardBackgroundChoice();
                                    break;
                                case R.id.settingsCardBackground4:
                                    choices = 4;
                                    selectedBackground = 4;
//                                    Card.updateCardBackgroundChoice();
                                    break;


                            }
                            if(choices < 5) {
                                prefs.saveCardBackground(choices);
                                Card.updateCardBackgroundChoice();
                            }else{
                                Log.d("Bamp: ","Pizdec");
                            }
                            dialog2.cancel();


                        }
                    });
                }

                for (int i = 0; i < NUMBER_OF_CARD_BACKGROUNDS; i++) {
                    linearLayoutsBackgrounds[i].setBackgroundResource(i == selectedBackground ? R.drawable.settings_highlight : typedValue.resourceId);
                }

                for (int i = 0; i < NUMBER_OF_CARD_BACKGROUNDS; i++) {
                    imageViews[i].setImageBitmap(bitmaps.getCardBack(i, selectedBackgroundColor));
                }
                ////////////////////////////////////////////////////////////////////////


                int row = prefs.getSavedFourColorMode() ? 1 : 0;

                linearLayouts[0] = (LinearLayout) dialog2.findViewById(R.id.settingsLinearLayoutCardsClassic);
                linearLayouts[1] = (LinearLayout) dialog2.findViewById(R.id.settingsLinearLayoutCardsAbstract);
                linearLayouts[2] = (LinearLayout) dialog2.findViewById(R.id.settingsLinearLayoutCardsOxygenDark);
                linearLayouts[3] = (LinearLayout) dialog2.findViewById(R.id.settingsLinearLayoutCardsOxygenLight);
                linearLayouts[4] = (LinearLayout) dialog2.findViewById(R.id.settingsLinearLayoutCardsPoker);

                for (int i = 0; i < NUMBER_OF_CARD_THEMES; i++) {
//                    linearLayouts[i].setOnClickListener((View.OnClickListener) getActivity());
                    ImageView imageView = (ImageView) linearLayouts[i].getChildAt(0);
                    imageView.setImageBitmap(bitmaps.getCardPreview(i, row));
                }

                for (int i = 0; i < NUMBER_OF_CARD_THEMES; i++) {
                    linearLayouts[i].setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            int choice;



                            switch (v.getId()) {
                                case R.id.settingsLinearLayoutCardsClassic:
                                default:
                                    choice = 2;
                                    // dialog.cancel();
                                    // gameManager.onResume();
                                    break;
                                case R.id.settingsLinearLayoutCardsAbstract:
                                    choice = 3;
                                    // dialog.cancel();
                                    // gameManager.onResume();
                                    break;
                                case R.id.settingsLinearLayoutCardsOxygenDark:
                                    choice = 6;
                                    break;
                                case R.id.settingsLinearLayoutCardsOxygenLight:
                                    choice = 7;
                                    break;
                                case R.id.settingsLinearLayoutCardsPoker:
                                    choice = 8;
                                    break;
                            }

                            prefs.saveCardTheme(choice);

                            Card.updateCardDrawableChoice();
                            dialog2.cancel();

                        }
                    });

                }

                dialog2.show();
            }
        });

        Button restart = (Button) dialog.findViewById(R.id.restart);
        restart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefs.getShowDialogRedeal()) {
                    prefs.putShowDialogRedeal(false);
                    DialogRedeal dialogRedeal = new DialogRedeal();
                    dialogRedeal.show(getFragmentManager(), "REDEAL_DIALOG");
                    dialog.cancel();
                } else {
                    gameLogic.redeal();
                    dialog.cancel();
                }
            }
        });

        Button buttonHelp = (Button) dialog.findViewById(R.id.how2);
        buttonHelp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog1 = new Dialog(getContext());
                dialog1.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog1.setContentView(R.layout.pager_layout);

                List<PagerModel> pagerArr = new ArrayList<>();
                pagerArr.add(new PagerModel("1","Rules\n\n" +  getResources().getString(R.string.manual_Klondike_rules)));
                pagerArr.add(new PagerModel("2","Objective\n\n" +  getResources().getString(R.string.manual_Klondike_objective)));
                pagerArr.add(new PagerModel("3","Structure\n\n" +  getResources().getString(R.string.manual_Klondike_structure)));
                pagerArr.add(new PagerModel("4","Scoring\n\n" +  getResources().getString(R.string.manual_Klondike_scoring)));


                PagerAdapter adapter = new PagerAdapter(getContext(),pagerArr);
                ViewPager pager = (ViewPager) dialog1.findViewById(R.id.pager);
                pager.setAdapter(adapter);

                ImageButton button = (ImageButton) dialog1.findViewById(R.id.button2);
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog1.cancel();
                    }
                });

                CirclePageIndicator pageIndicator = (CirclePageIndicator) dialog1.findViewById(R.id.page_indicator);
                pageIndicator.setViewPager(pager);
                pageIndicator.setCurrentItem(0);

                dialog1.show();
            }
        });

        Button newB = (Button) dialog.findViewById(R.id.new2);
        newB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prefs.getShowDialogNewGame()) {
                    prefs.putShowDialogNewGame(false);
                    DialogStartNewGame dialogStartNewGame = new DialogStartNewGame();
                    dialogStartNewGame.show(getFragmentManager(), "START_NEW_GAME_DIALOG");
                    dialog.cancel();
                } else {
                    gameLogic.newGame();
                    dialog.cancel();
                }
            }
        });

        return dialog;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            // When an Image is picked
            // Get the Image from data

            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            // Get the cursor
            Cursor cursor = getActivity().getContentResolver().query(selectedImage, filePathColumn, null, null, null);
            // Move to first row
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            imgDecodableString = cursor.getString(columnIndex);
            cursor.close();

            ImageView imgView = (ImageView) getActivity().findViewById(R.id.mainRelativeLayoutBackground);
            // Set the Image in ImageView after decoding the String
//                imgView.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
            imgView.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));

            SharedPreferences editor = getActivity().getSharedPreferences(PREFERENCES_CONSTANTS, MODE_MULTI_PROCESS);
            editor.edit().putString(BACKGROUND_PREFERENCES_URL, imgDecodableString).apply();
            editor.edit().putBoolean(BACKGROUND_PREFERENCES_USER, true).apply();
            editor.edit().putBoolean(BACKGROUND_PREFERENCES_BACK, false).apply();
            imgDecodableString = mSettings.getString(BACKGROUND_PREFERENCES_URL, "");
            // imgView.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
            // Drawable d = new BitmapDrawable(BitmapFactory.decodeFile(imgDecodableString));
            // Log.d("TAG:d ", String.valueOf(d));
            imgView.setBackground(Drawable.createFromPath(imgDecodableString));
            Log.d("TAG:d  ", imgDecodableString);

            //editor.apply();


        } catch (Exception e) {
            Toast.makeText(getActivity(), "Something went wrong", Toast.LENGTH_LONG).show();
        }

    }
}