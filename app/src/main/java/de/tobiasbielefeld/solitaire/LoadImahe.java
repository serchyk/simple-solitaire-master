package de.tobiasbielefeld.solitaire;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import static de.tobiasbielefeld.solitaire.dialogs.DialogInGameHelpMenu.BACKGROUND_PREFERENCES_URL;

public class LoadImahe extends Activity {
    private static int RESULT_LOAD_IMG = 1;
    String imgDecodableString;
    SharedPreferences mSettings;
    public static final String BACKGROUND_PREFERENCES_USER = "USER";

    public static final String BACKGROUND_PREFERENCES_BACK = "BACK";
    public static final String PREFERENCES_CONSTANTS = "fuck";

    static final String TAG = "LOG:";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.imageactiv);
    }

    public void loadImagefromGallery(View view) {
        // Create intent to Open Image applications like Gallery, Google Photos
        Intent galleryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        // Start the Intent
        startActivityForResult(galleryIntent, RESULT_LOAD_IMG);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == RESULT_LOAD_IMG && resultCode == RESULT_OK && null != data) {
                // Get the Image from data

                Uri selectedImage = data.getData();
                String[] filePathColumn = {MediaStore.Images.Media.DATA};

                // Get the cursor
                Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                // Move to first row
                cursor.moveToFirst();

                int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                imgDecodableString = cursor.getString(columnIndex);
                cursor.close();

                ImageView imgView = (ImageView) findViewById(R.id.imgViews);
                // Set the Image in ImageView after decoding the String
                imgView.setImageBitmap(BitmapFactory.decodeFile(imgDecodableString));
                Log.d(TAG, imgDecodableString);
                SharedPreferences editor = getSharedPreferences(PREFERENCES_CONSTANTS,MODE_MULTI_PROCESS);
                editor.edit().putString(BACKGROUND_PREFERENCES_URL, imgDecodableString).apply();
                editor.edit().putBoolean(BACKGROUND_PREFERENCES_USER, true).apply();
                //editor.apply();

            } else {
                Toast.makeText(this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
        }

    }
}
